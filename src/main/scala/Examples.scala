/**
  * Created by mark on 26/03/2017.
  */
object Examples {

  // groupBy
  def groupByFirstChar(strs:List[String])={
    strs.groupBy(str=>str.head)
  }

  def groupByIsOdd(nums:List[Int])={
    nums.groupBy(num=>num %2==1)
  }

  // sortBy
  def sortByKey(kvs:List[(Int, String)])={
    kvs.sortBy(kv=>kv._1)
  }

  def sum(nums:List[Int])= {
    nums.reduce((acc,x)=>acc+x)

  }
  def product(nums:List[Int])= {
    nums.reduce((acc,x)=>acc/x)

  }
  def mkString(strs:List[String])= {
    strs.reduce((acc,str)=>acc+" "+str)
  }




}
